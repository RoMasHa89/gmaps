var geocoder;
var map;
var mapOptions = {
    zoom: 16,
};
var marker;
var bounds = new google.maps.LatLngBounds(null);

function initializes() {
    //var myLatlng = new google.maps.LatLng(50.0, 36.0);
    map = new google.maps.Map(document.getElementById("shops_canvas"),
        mapOptions);

    for(var i = 0; i <   data.length; i++){
        add_marker(data[i].address, data[i].title, data[i].description);

    }

}

jQuery(document).ready(function($) {
        initializes();
        jQuery('#shops_canvas').css({height: 400});
    });

function add_marker(address, title, description) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
            myLatlng = results[0].geometry.location;

            map.setCenter(results[0].geometry.location);
            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: title
            });

            var boxText = document.createElement("div");
            boxText.id = 'infobox';

            boxText.innerHTML = "\
                <div><b>" + localize.title_text + "</b><br>&emsp;" + title + "</div>\
                <div><b>" + localize.desc_text + "</b><br>&emsp;" + description + "</div>";

            var infowindow = new google.maps.InfoWindow({
                content: boxText,
                position: myLatlng
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,this);
                map.setCenter(this.getPosition());
            });

            bounds.extend(myLatlng);
            map.fitBounds(bounds);
            map.panToBounds(bounds);
            var zoom = map.getZoom();
            map.setZoom(zoom > 17 ? 17 : zoom);

        } else {

            res = false;
        }
    });
}