<?php
/*
Plugin Name: Gmaps Shop List
Plugin URI: --
Description: Plugin, that provides shop list on GoogleMaps. It's has an shortcode to show map with all shops on the map (example: [shops-shortcode])
Version: 1.0.0
Author: Alex Romantsov
Author URI: --
Text Domain: shop_gmaps
Domain Path: /languages
License: No Licence
*/

// Create new post type
function create_shop_gmaps() {
    register_post_type( 'shop_gmaps',
        array(
            'labels' => array(
                'name' => __('Shops', 'shop_gmaps'),
                'singular_name' => __('Shops', 'shop_gmaps'),
                'add_new' => __('Add New', 'shop_gmaps'),
                'add_new_item' => __('Add New Shop', 'shop_gmaps'),
                'edit' => __('Edit', 'shop_gmaps'),
                'edit_item' => __('Edit Shops', 'shop_gmaps'),
                'new_item' => __('New Shop', 'shop_gmaps'),
                'view' => __('View', 'shop_gmaps'),
                'view_item' => __('View Shops', 'shop_gmaps'),
                'search_items' => __('Search Shops', 'shop_gmaps'),
                'not_found' => __('No Shops found', 'shop_gmaps'),
                'not_found_in_trash' => __('No Shops Reviews found in Trash', 'shop_gmaps'),
                'parent' => __('Parent Shops', 'shop_gmaps')
            ),
            'public' => true,
            'menu_position' => 15,
			'show_ui' => true,
			'_builtin' => false,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array("slug" => "shops"),
			'supports' => array('title', 'editor'),
			'menu_icon'   => 'dashicons-location-alt'
        )
    );

}

add_action( 'init', 'create_shop_gmaps' );


// creates custom fields on post-type 'shop_gmaps'
function shop_gmaps_add_custom_box() {
	$screens = array( 'shop_gmaps');
	foreach ( $screens as $screen )
		add_meta_box( 'shops_sectionid', __('Shops options:', 'shop_gmaps'), 'shop_gmaps_meta_box_callback', $screen );
}
add_action('add_meta_boxes', 'shop_gmaps_add_custom_box');


// html layout of custom fields for post-type 'shop_gmaps'
function shop_gmaps_meta_box_callback() {
	// using nonce for verify
	wp_nonce_field( plugin_basename(__FILE__), 'shop_gmaps' );


     // out fields
     $my_fields = array('_shop_description_value_key' => '', '_shop_address_value_key' => '');

     foreach ($my_fields as $key => $value) {
             $my_fields[$key] = get_post_meta(get_the_ID(), $key, true);
     }

	?>
	<!-- fields for data input -->
	<label for="shop_description"><?php echo __('Description', 'shop_gmaps')?></label> :
	<input type="text" id= "shop_description" name="shop_description" value=" <?php echo $my_fields['_shop_description_value_key']?>" size="80" />
	<br>
		
	<label for="shop_address"><?php echo __('Address', 'shop_gmaps')?></label> :
	<input type="text" id= "shop_address" name="shop_address" value=" <?php echo $my_fields['_shop_address_value_key']?>" size="80" />
	<?php
}

function gmap_script_enqueuer() {
	wp_enqueue_script('gmap-api-script', 'https://maps.googleapis.com/maps/api/js?v=3.exp');
	wp_register_script('gmap-script',  plugins_url('/js/gmaps.js', __FILE__), array('jquery'), '1.0', true );

}
add_action( 'wp_enqueue_scripts', 'gmap_script_enqueuer' );

// Save data when post is getting saved
function shop_gmaps_save_postdata( $post_id ) {
	// check nonce of our page for calling from our "shop_gmaps" form
	if ( ! wp_verify_nonce( $_POST['shop_gmaps'], plugin_basename(__FILE__) ) )
		return $post_id;

	// check for autosave, if true - skip saving data
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return $post_id;

	// check user capability to edit post
	if ( 'page' == $_POST['post_type'] && ! current_user_can( 'edit_page', $post_id ) ) {
		  return $post_id;
	} elseif( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	// check fields is set
	if ( ! isset( $_POST['shop_description'] ) && ! isset( $_POST['shop_address'] ))
		return;

	// Getting data from fields and updating database
	$my_data = sanitize_text_field( $_POST['shop_description'] );
	update_post_meta( $post_id, '_shop_description_value_key', $my_data );

	//Getting data from fields and updating database
	$my_data = sanitize_text_field( $_POST['shop_address'] );
	update_post_meta( $post_id, '_shop_address_value_key', $my_data );

}
add_action( 'save_post', 'shop_gmaps_save_postdata', 1, 2 );


// Add a shortcode for "shop_gmaps" post type
function shop_gmaps_shortcode_func( $atts ) {
	// Getting arguments
	extract( $atts );

	// Getting data of 'news' post type from database
	$mypost = array( 'post_type' => 'shop_gmaps', 'posts_per_page' => -1);
	$loop = new WP_Query( $mypost );

	// Data array
	$data = array();

	// if count of posts is greater then 0, starting to print posts content on page
	while ( $loop->have_posts()) :

		// Goto next post
		$loop->the_post();

		// Getting data from custom fields of post
		array_push($data,
				array(
					'address' => get_post_meta( get_the_ID(), '_shop_address_value_key', true ),
					'title' => get_the_title(),
					'description'	 => get_post_meta( get_the_ID(), '_shop_description_value_key', true )
				)
		);
	endwhile;

	// Store shops list
	$data['length'] = $loop->post_count;

	// Container for map
	echo '<div id="shops_canvas" class="shops_canvas">&emsp;</div>';

	//Sending data to JS
	wp_localize_script( 'gmap-script', 'data', $data);
	wp_localize_script( 'gmap-script', 'localize', array(
			'title_text' => __( 'Title:', 'shop_gmaps' ),
			'desc_text' => __( 'Description:', 'shop_gmaps' )
	) );
	wp_enqueue_script( 'gmap-script' );
}
add_shortcode( 'shops-shortcode', 'shop_gmaps_shortcode_func' );


function is_post_type($type){
	global $wp_query;
	if($type == get_post_type($wp_query->post->ID)) return true;
	return false;
}

function shop_gmaps_post_action(  $content ) {

	// check if out post is post_type named 'shop_gmaps', and viewing a single post
	if (is_single() && is_post_type('shop_gmaps')){

		// Data array
		$data = array();

		// Getting data from custom fields of post
		array_push($data,
				array(
						'address' => get_post_meta( get_the_ID(), '_shop_address_value_key', true ),
						'title' => get_the_title(),
						'description'	 => get_post_meta( get_the_ID(), '_shop_description_value_key', true )
				)
		);

		// Store shops list
		$data['length'] = 1;

		// Container for map
		echo '<div id="shops_canvas" class="shops_canvas"></div>';

		//Sending data to JS
		wp_localize_script( 'gmap-script', 'data', $data);
		wp_localize_script( 'gmap-script', 'localize', array(
				'title_text' => __( 'Title:', 'shop_gmaps' ),
				'desc_text' => __( 'Description:', 'shop_gmaps' )
		) );
		wp_enqueue_script( 'gmap-script' );
	} else return $content;

}
add_filter('the_content','shop_gmaps_post_action');


function shop_gmaps_load_plugin_textdomain() {
	load_plugin_textdomain( 'shop_gmaps', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'shop_gmaps_load_plugin_textdomain' );

?>

